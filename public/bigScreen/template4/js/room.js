//计划完成表的当前所选
var indexnum = 0;
var color = ['#F35331', '#2499F8', '#3DF098', '#33B734'];
var fontColor = '#FFF';

//定义进度条组件和属性
var y_gauge1 = null;
var y_gauge2 = null;
var y_gauge3 = null;
var y_gauge4 = null;
var m_gauge1 = null;
var cd_num = null;
var cd_money = null;
var option_Progress = null;
var option_water = null;
//订单情况螺旋图
var orderStatus = null;
var orderStatus_option = null;

//定义仪表盘组件和属性
var gauge1 = null;
var gauge2 = null;
var gauge3 = null;
var gauge4 = null;
var gauge5 = null;
var option_gauge = null;

//产品饼图组件和属性
var productPie = null;
var productPie_option = null;

//业务进展图组件和属性
var businessProgress = null;
var businessProgress_placeHoledStyle = null;
var businessProgress_dataStyle = null;
var businessProgress_option = null;

//生产质量堆积图组件和属性
var quality_chart = null;
var quality_option = null;

//词云组件和属性
var wordCloud = null;
var wordCloud_option = null;

//生产计划折线图组件和属性
var plan_chart = null;
var plan_option = null;
var map = null;

//环形图的风格定义
var dataStyle = {
	normal: {
		label: { show: false },
		labelLine: { show: false }
	}
};
var placeHolderStyle = {
	normal: {
		color: 'rgba(0,0,0,0.1)',
		label: { show: false },
		labelLine: { show: false }
	},
	emphasis: {
		color: 'rgba(0,0,0,0)'
	}
};

// 水位图
// const option = {
//     series: [{
//         type: 'liquidFill',
//         data: [0.6]
//     }]
// };

//最大订单号
var lastOrderNumber = 1;

//电量数据
var dlData = [
	{ date: '2021-03', zdl: '289608000', zlxl: '97.37%', syb: '184348', jhyd: '2131964', tlyd: '458073', kyjyd: '1222748', yfxt: '3.8' },
	{ date: '2021-04', zdl: '281721000', zlxl: '97.16%', syb: '209842', jhyd: '2290676', tlyd: '439337', kyjyd: '1055955', yfxt: '3.9' },
	{ date: '2021-05', zdl: '292116000', zlxl: '97.62%', syb: '269194', jhyd: '2445082', tlyd: '600035', kyjyd: '1049657', yfxt: '3.9' },
	{ date: '2021-06', zdl: '285285000', zlxl: '97.74%', syb: '304149', jhyd: '2379431', tlyd: '632042', kyjyd: '993711', yfxt: '4.3' },
	{ date: '2021-07', zdl: '292578000', zlxl: '97.89%', syb: '314166', jhyd: '2390681', tlyd: '517908', kyjyd: '1029275', yfxt: '3.6' },
	{ date: '2021-08', zdl: '292611000', zlxl: '97.98%', syb: '308771', jhyd: '2396809', tlyd: '651824', kyjyd: '1056308', yfxt: '3.5' },
	{ date: '2021-09', zdl: '286044000', zlxl: '97.93%', syb: '262029', jhyd: '2300906', tlyd: '826611', kyjyd: '1052021', yfxt: '3.8' },
	{ date: '2021-10', zdl: '298716000', zlxl: '97.99%', syb: '227293', jhyd: '2503407', tlyd: '616820', kyjyd: '1123486', yfxt: '3.7' },
	{ date: '2021-11', zdl: '285516000', zlxl: '97.93%', syb: '193287', jhyd: '2515094', tlyd: '621994', kyjyd: '1045940', yfxt: '3.6' },
	{ date: '2021-12', zdl: '293964000', zlxl: '97.91%', syb: '182367', jhyd: '2721139', tlyd: '732345', kyjyd: '1100726', yfxt: '4.1' },
	{ date: '2022-01', zdl: '292446000', zlxl: '97.97%', syb: '178525', jhyd: '2526805', tlyd: '663159', kyjyd: '1100833', yfxt: '4.2' },
	{ date: '2022-02', zdl: '265221000', zlxl: '97.97%', syb: '159047', jhyd: '2278053', tlyd: '846719', kyjyd: '947605', yfxt: '4.5' },
	{ date: '2022-03', zdl: '294657000', zlxl: '97.91%', syb: '186317', jhyd: '2326757', tlyd: '718781', kyjyd: '1026368', yfxt: '4.4' }
]
//风压
var fyData = [
	{ date: '01:00', value: "4.0" },
	{ date: '02:00', value: "4.0" },
	{ date: '03:00', value: "4.0" },
	{ date: '04:00', value: "3.9" },
	{ date: '05:00', value: "4.0" },
	{ date: '06:00', value: "4.0" },
	{ date: '07:00', value: "3.8" },
	{ date: '08:00', value: "3.9" },
	{ date: '09:00', value: "4.0" },
	{ date: '10:00', value: "3.9" },
	{ date: '11:00', value: "4.0" }, 
	{ date: '12:00', value: "3.9" },
	{ date: '13:00', value: "4.0" },
	{ date: '14:00', value: "3.9" },
	{ date: '15:00', value: "4.0" },
	{ date: '16:00', value: "4.0" },
	{ date: '17:00', value: "4.0" },
	{ date: '18:00', value: "3.9" },
	{ date: '19:00', value: "3.9" },
	{ date: '20:00', value: "4.0" },
	{ date: '21:00', value: "3.9" },
	{ date: '22:00', value: "4.0" },
	{ date: '23:00', value: "3.9" },
	{ date: '00:00', value: "4.0" },
]; 
$(document).ready(function () {
	map = new BMapGL.Map("mapDiv");          // 创建地图实例
	var point = new BMapGL.Point(112.51078, 34.70431);  // 创建点坐标
	map.setMapStyleV2({styleJson: styleJson2});
	map.centerAndZoom(point, 15);
	map.enableScrollWheelZoom(true);     //开启鼠标滚轮缩放
	map.setHeading(64.5);   //设置地图旋转角度
	//map.setTilt(73);       //设置地图的倾斜角度

	loadLampData();
	loadSbzl();
 	loadHx();
 
	//订单完成情况螺旋图  
	//loadJhydData();

	//loadHz
	loadGdsydData();
	loadBjygb();

	//loadTlydData();
	//loadKyjydData();
	//轮番显示tips
	/*function clock(){
	  showToolTip_highlight(plan_chart);	  
	}
	setInterval(clock, 5000);*/

	 
	resresh();

	//开始定时刷新
	setInterval(resresh, 5 * 1000);
	setInterval(setTimeLoad, 30 * 1000);
});
var setTimeLoad= function(){
	loadSbzl();
	loadLampData();
}
var loadLampData =function () {
	var base_url = window._CONFIG['domianURL'];
  var token = JSON.parse(localStorage.getItem('pro__Access-Token')).value
	$.ajax({
		type : 'POST',
		async: false,   // async, avoid js invoke pagelist before jobId data init
		url : base_url + '/corp/lamp/allList',
		data : {},
		dataType : "json",
    beforeSend:function(request){
      request.setRequestHeader("X-Access-Token",token);
    },
		success : function(data){
			if (data.code == 200) {

				let lampData = data.result;
				if(lampData.length>0){
					var point=new BMapGL.Point(lampData[0].gwlongitude, lampData[0].gwlatitude);
					map.centerAndZoom(point, 15);
				}
				map.clearOverlays(); //删除所有点
				for (var i = 0; i < data.result.length; i++) {
					// 创建一个点
					// var point1 = new BMapGL.Point(data.result[i].gwlongitude,data.result[i].gwlatitude);
					// var marker = new BMapGL.Marker(point1,{ enableDragging: true});// 创建标注
					// map.addOverlay(marker);

					let lamp = lampData[i];

					let marker = 'marker' + i; //动态生成变量名
					let point = 'point' + i; //动态生成变量名
					let opts = 'opts' + i; //动态生成变量名
					let infoWindow = 'infoWindow' + i; //动态生成变量名
					// 创建点标记
					window[point] = new BMapGL.Point(lamp.gwlongitude,lamp.gwlatitude); //给变量赋值
					// 在地图上添加点标记
					window[marker] = new BMapGL.Marker(window[point]); //给变量赋值
					map.addOverlay(window[marker]); //使用变量
					// 创建信息窗口
					window[opts] = {
						width: 200,
						height: 100,
						title: lamp.name,
						panel : "panel", //检索结果面板
						enableAutoPan : true, //自动平移
						searchTypes :[
						]
					};
					var btnText ="未知";
					var statusText='';
					if (lamp.onStatus=='N'){
						btnText="开";
						statusText="已关闭";
					}
					if (lamp.onStatus=='Y') {
						btnText="关";
						statusText="已开启";
					}
					var content ='<div style="font-size: 10px;">编码：' + lamp.code +'<br>开关状态：'+ statusText +'<button style="float:right;" class="switch-btn" onclick="doSwitch(\''+lamp.id+'\',\''+lamp.onStatus+'\')">'+btnText+'</button></div>';
					window[infoWindow] = new BMapGL.InfoWindow(content, window[opts]);//使用变量
					// 点标记添加点击事件
					window[marker].addEventListener('click', function () {
						map.openInfoWindow(window[infoWindow], window[point]); // 开启信息窗口
					});
				}


			} else {

			}
		},
	});
}
var doSwitch =function (id,onStatus) {
	if(confirm("确认要"+(onStatus == 'N'?'开启':'关闭')+"?")) {
		var base_url = "/jeecg-boot";
		$.ajax({
			type: 'GET',
			async: false,   // async, avoid js invoke pagelist before jobId data init
			url: base_url + '/corp/lamp/control?id=' + id + '&on=' + (onStatus == 'N' ? '1' : '0'),
			data: {},
			dataType: "json",
			success: function (data) {
				if (data.code == 200) {
					alert("操作成功！");
				} else {

				}
			},
		});
	}
}
var loadHz=function(){
	//产品销售的环形图
	var productLegend = [['轿车1', '轿车2', '轿车3', '轿车4', '轿车5', '轿车6', '轿车7', '轿车8', '轿车9'], ['SUV1', 'SUV2', 'SUV3', 'SUV4'], ['卡车1', '卡车2', '卡车3', '卡车4', '卡车5', '卡车6']];
	var productClassLegend = ['轿车', 'SUV', '卡车'];
	var productClassColor = ['rgba(255,153,0,', 'rgba(153,204,102,', 'rgba(0,102,255,'];
	var productClassData = [];
	var productData = [];
	var productColor = [];
	for (var i = 0; i < productClassLegend.length; i++) {
		var total = 0;
		for (var j = 0; j < productLegend[i].length; j++) {
			var n = Math.round(Math.random() * 100) + 1;
			productData.push({ name: productLegend[i][j], value: n });
			total += n;
		}
		for (var j = 0; j < productLegend[i].length; j++) {
			productColor.push(productClassColor[i] + (1.0 - productData[j].value / total).toFixed(2) + ")");
		}
		productClassData.push({ name: productClassLegend[i], value: total });
	}

	productPie = echarts.init(document.getElementById('productPie'));
	productPie_option = {
		title: {
			text: '销售额',
			x: 'center',
			y: 'center',
			itemGap: 10,
			textStyle: {
				color: '#09F',
				fontWeight: 'normal',
				fontFamily: '微软雅黑',
				fontSize: 32
			}
		},
		calculable: false,
		tooltip: {
			trigger: 'item',
			textStyle: {
				color: '#FFF',
				fontSize: 24
			},
			formatter: "{a} <br/>{b} : {c} ({d}%)"
		},
		series: [
			{
				name: '类别',
				type: 'pie',
				selectedMode: 'single',
				radius: ['20%', '40%'],
				width: '40%',
				funnelAlign: 'right',
				itemStyle: {
					normal: {
						color: function (d) {
							return productClassColor[d.dataIndex] + '1)';
						},
						borderColor: '#032749',
						label: {
							position: 'inner',
							fontSize: 28,
						},
						labelLine: {
							show: false
						}
					}
				},
				data: productClassData
			},
			{
				name: '车型',
				type: 'pie',
				radius: ['40%', '70%'],
				width: '35%',
				funnelAlign: 'left',
				itemStyle: {
					normal: {
						color: function (d) {
							return productColor[d.dataIndex];
						},
						borderColor: '#032749',
						label: {
							color: '#B7E1FF',
							fontSize: 24
						}
					}
				},
				data: productData
			}
		]
	};
	productPie.setOption(productPie_option);

}

var loadHx =function(){
	
	//环形进度条设置对象	
	option_Progress = {
		title: {
			text: '目前进度',
			subtext: '50%',
			x: 'center',
			y: 55,
			itemGap: 10, 
			textStyle: {
				color: '#0bddfc',
				fontWeight: 'normal',
				fontFamily: '微软雅黑',
				fontSize: 16
			},
			subtextStyle: {
				color: '#0bddfc',
				fontWeight: 'bolder',
				fontSize: 14,
				fontFamily: '微软雅黑'
			},
		
		},
		series: [{
			type: 'pie',
			center: ['50%', '50%'],
			radius: [35, 50],
			x: '0%',
			tooltip: { show: false },
			data: [{
				name: '达成率',
				value: 79,
				itemStyle: { color: 'rgba(0,153,255,0.8)' },
				hoverAnimation: false,
				label: {
					show: false,
					position: 'center',
					textStyle: {
						fontFamily: '微软雅黑',
						fontWeight: 'bolder',
						color: '#B7E1FF',
						fontSize: 24
					}
				},
				labelLine: {
					show: false
				}
			},
			{
				name: '79%',
				value: 21,
				itemStyle: { color: 'rgba(0,153,255,0.1)' },
				hoverAnimation: false,
				label: {
					show: false,
					position: 'center',
					padding: 20,
					textStyle: {
						fontFamily: '微软雅黑',
						fontSize: 24,
						color: '#B7E1FF'
					}
				},
				labelLine: {
					show: false
				}
			}]
		},
		{
			type: 'pie',
			center: ['50%', '50%'],
			radius: [55, 60],
			x: '0%',
			hoverAnimation: false,
			data: [{
				value: 100,
				itemStyle: { color: 'rgba(0,153,255,0.3)' },
				label: { show: false },
				labelLine: { show: false }
			}]
		},
		{
			type: 'pie',
			center: ['50%', '50%'],
			radius: [19, 20],
			x: '0%',
			hoverAnimation: false,
			data: [{
				value: 100,
				itemStyle: { color: 'rgba(0,153,255,0.3)' },
				label: { show: false },
				labelLine: { show: false }
			}]
		}]
	};

	option_water = {
		 series: [{
		        type: 'liquidFill',
		        data: [0.8],
				label:{
					fontSize:19,
					color:'#fff'
				},
				color:['#5BC5F6'],
				backgroundStyle:{
					color:"#153067"
				},
				outline: {
				        }
		    }]
	}
	option_water2 = {
		 series: [{
		        type: 'liquidFill',
		        data: [0.4],
				label:{
					fontSize:19,
					color:'#fff',
					formatter:'100元'
					
				},
				color:['#DF983D'],
				backgroundStyle:{
					color:"#153067"
				},
				outline: {
				        }
		    }]
	}
	
	//
	y_gauge1 = echarts.init(document.getElementById('y_gauge1')); 
	y_gauge2 = echarts.init(document.getElementById('y_gauge2')); 
	y_gauge3 = echarts.init(document.getElementById('y_gauge3')); 
	cd_num = echarts.init(document.getElementById('cd_num')); 
	cd_money = echarts.init(document.getElementById('cd_money')); 
}

var convertData = function (data) {
	var res = [];
	for (var i = 0; i < data.length; i++) {
		var dataItem = data[i];
		var fromCoord = geoCoordMap[dataItem[0].name];
		var toCoord = geoCoordMap[dataItem[1].name];
		if (fromCoord && toCoord) {
			res.push({
				fromName: dataItem[0].name,
				toName: dataItem[1].name,
				coords: [fromCoord, toCoord]
			});
		}
	}
	return res;
};

function showToolTip_highlight(mychart) {
	var echartObj = mychart;

	// 高亮当前图形
	var highlight = setInterval(function () {
		echartObj.dispatchAction({
			type: 'highlight',
			seriesIndex: 0,
			dataIndex: indexnum
		});

		echartObj.dispatchAction({
			type: 'showTip',
			seriesIndex: 0,
			dataIndex: indexnum
		});
		clearInterval(highlight);
		indexnum = indexnum + 1;
		if (indexnum >= 7) indexnum = 0;
	}, 1000);
}

//定时刷新数据
function resresh() {
	var myDate = new Date();

	// $('#refresh').html("<img src=\"images/wait.gif\" align=\"absmiddle\"><span>数据刷新中...</span>");
	$('#currentDate').html(myDate.getFullYear() + "/" + insertZero(myDate.getMonth() + 1) + "/" + insertZero(myDate.getDate()));
	$('#timeYear').html(myDate.getFullYear());
	$('#timeMonth').html(insertZero(myDate.getMonth() + 1));
	$('#timeDay').html(insertZero(myDate.getDate()));
	var week;
	switch (myDate.getDay()) {
		case 1:
			week = "一";
			break;
		case 2:
			week = "二";
			break;
		case 3:
			week = "三";
			break;
		case 4:
			week = "四";
			break;
		case 5:
			week = "五";
			break;
		case 6:
			week = "六";
			break;
		case 0:
			week = "日";
			break;
		default:
			break;
	}
	$('#timeWeekday').html(week);
	//+":" + insertZero(myDate.getSeconds())); 

	//显示最后更新时间
	//$('#refresh').html("<span id=\"refreshTime\">最后刷新时间："+myDate.toLocaleDateString()+" "+myDate.toLocaleTimeString()+"</span>");
	var maxg=100;
	var n1=Math.round(Math.random()*100).toFixed(2);
	var n2=Math.round(Math.random()*100).toFixed(2);
	var n3=Math.round(Math.random()*100).toFixed(2);
	
	//年进度条
	option_Progress.title.subtext ="亮灯率";
	option_Progress.series[0].data[0].value = n1;
	option_Progress.title.text =n1+"%";
	option_Progress.series[0].data[1].value =(maxg-n1);
	y_gauge1.setOption(option_Progress);

	option_Progress.title.subtext ="节能率";
	option_Progress.series[0].data[0].value = n2;
	option_Progress.title.text=n2+"%";
	option_Progress.series[0].data[1].value =(maxg-n2);
	y_gauge2.setOption(option_Progress);
	
	option_Progress.title.subtext ="在线率";
	option_Progress.series[0].data[0].value = n3;
	option_Progress.title.text =n3+"%";
	option_Progress.series[0].data[1].value =(maxg-n3);
	y_gauge3.setOption(option_Progress);
	option_water.series[0].data[0] = 0.5;
	option_water.series[0].color[0] = '#5BC5F6'
	cd_num.setOption(option_water);
	cd_money.setOption(option_water2);
}

function loadJhydData() {
	var jhydData = [];//总电量 
	var sybData = [];//所用变（千瓦时） 
	var xAxisData = [];
	for (var i = 0; i < dlData.length; i++) {
		var item = dlData[i];
		jhydData.push(item.jhyd);
		sybData.push(item.syb);
		xAxisData.push(item.date);
	}
	orderStatus = echarts.init(document.getElementById('orderStatus'));
	orderStatus_option = {
		title: { show: false },

		tooltip: {
			trigger: 'axis',
			textStyle: {
				color: '#FFF',
				fontSize: 20
			}
		},
		toolbox: { show: false },
		legend: {
			show: false,
			top: 'top',
			textStyle: {
				color: '#B7E2FF',
				fontSize: 20,
				fontFamily: '微软雅黑'
			},
			data: ['电量']
		},
		xAxis: {
			data: xAxisData,
			axisLabel: {
				textStyle: {
					color: '#B7E1FF',
					fontSize: 15
				}
			},
			axisLine: {
				lineStyle: {
					color: '#09F'
				}
			},
			axisTick: {
				lineStyle: {
					color: '#09F'
				}
			}
		},
		yAxis: {
			min: 2000000,
			inverse: false,
			splitArea: { show: false },
			axisLine: { show: false },
			axisTick: { show: false },
			axisLabel: {
				textStyle: {
					color: '#B7E1FF',
					fontSize: 15,
					fontFamily: 'Arial',
				}
			},
			splitLine: {
				lineStyle: {
					color: '#09F'
				}
			}
		},
		grid: {
			left: 100
		},
		series: [
			{
				name: '电量',
				type: 'line',
				smooth: true,
				symbol: 'circle',
				symbolSize: 6,
				showAllSymbol: true,
				color: '#F90',
				itemStyle: {
					normal: {
						lineStyle: {
							width: 1.7
						}
					}
				},
				data: jhydData
			},
			// {
			// 	name: '所用变',
			// 	type: 'line',
			// 	smooth: true,
			// 	symbol: 'circle',
			// 	symbolSize: 10,
			// 	showAllSymbol: true,
			// 	color: color[1],
			// 	data: sybData
			// },
		]
	}
	orderStatus.setOption(orderStatus_option);
}
// 设备总览
function loadSbzl() {
	// var base_url ="/jeecg-boot";
	var base_url =window._CONFIG['domianURL'];
  var token = JSON.parse(localStorage.getItem('pro__Access-Token')).value
	$.ajax({
		type : 'POST',
		async: false,   // async, avoid js invoke pagelist before jobId data init
		url : base_url + '/corp/lamp/deviceOverview',
		data : {},
		dataType : "json",
    beforeSend:function(request){
      request.setRequestHeader("X-Access-Token",token);
    },
		success : function(data){
			if (data.code == 200) {
				document.getElementById('sbzl-zm').innerText = data.result.zm;
				for(let key in data.result){
					document.getElementById('sbzl-'+key).innerText = data.result[key];
				}
			} else {

			}
		},
    fail:function(err){
      console.log(err)
    }
	});
}
function loadGdsydData() {
	//供电所用电
	var plan_data1 = [];
	var plan_data2 = [];
	var plan_xAxis = [];
	for (var i = 0; i < dlData.length; i++) {
		var item = dlData[i];
		plan_xAxis.push(item.date);
		//plan_data1.push(item.syb);
		//plan_data2.push(item.syb);
		plan_data2.push(item.yfxt);
	}

	plan_chart = echarts.init(document.getElementById('ggwfChart'));
	plan_option = {
		xAxis: {
			data: plan_xAxis,
			axisLabel: {
				textStyle: {
					color: '#B7E1FF',
					fontSize: 15
				}
			},
			axisLine: {
				lineStyle: {
					color: '#09F'
				}
			},
			axisTick: {
				lineStyle: {
					color: '#09F'
				}
			}
		},
		yAxis: {
			min: 3.5,
			max: 4.5,
			inverse: false,
			splitArea: { show: false },
			axisLine: { show: false },
			axisTick: { show: false },
			axisLabel: {
				textStyle: {
					color: '#B7E1FF',
					fontSize: 15,
					fontFamily: 'Arial',
				}
			},
			splitLine: {
				lineStyle: {
					color: '#09F'
				}
			}
		},
		tooltip: {
			trigger: 'axis',
			textStyle: {
				color: '#FFF',
				fontSize: 20
			}
		}, 
		legend: {
			show: false,
			top: 'bottom',
			textStyle: {
				color: '#F00',
				fontSize: 10
			},
			data: ['数据']
		},
		series: [
			{
				name: '数据',
				type: 'line',
				smooth: true,
				symbol: 'circle',
				symbolSize: 6,
				showAllSymbol: true,
				color: '#50fcfc',
				itemStyle: {
					normal: {
						lineStyle: {
							width: 1.7
						}
					}
				},
				data: plan_data2
			},

		]
	};
	plan_chart.setOption(plan_option);
}
//风压监控
function loadTlydData() {
	var plan_data1 = [];
	var plan_data2 = [];
	var plan_xAxis = [];

	for (var i = 0; i < fyData.length; i++) {
		var item = fyData[i];
		plan_xAxis.push(item.date);
		plan_data2.push(item.value);
	}

	businessProgress = echarts.init(document.getElementById('businessProgress'));
	businessProgress_option = {
		xAxis: {
			data: plan_xAxis,
			axisLabel: {
				textStyle: {
					color: '#B7E1FF',
					fontSize: 10
				}
			},
			axisLine: {
				lineStyle: {
					color: '#09F'
				}
			},
			axisTick: {
				lineStyle: {
					color: '#09F'
				}
			}
		},
		yAxis: {
			min: 3.5,
			inverse: false,
			splitArea: { show: false },
			axisLine: { show: false },
			axisTick: { show: false },
			axisLabel: {
				textStyle: {
					color: '#B7E1FF',
					fontSize: 15,
					fontFamily: 'Arial',
				}
			},
			splitLine: {
				lineStyle: {
					color: '#09F'
				}
			}
		},
		tooltip: {
			trigger: 'axis',
			textStyle: {
				color: '#FFF',
				fontSize: 20
			}
		},
		grid: {
			left: 100
		},
		legend: {
			show: false,
			top: 'bottom',
			textStyle: {
				color: '#F00',
				fontSize: 24
			},
			data: ['电量(千瓦时)']
		},
		series: [
			{
				name: '电量',
				type: 'line',
				smooth: true,
				symbol: 'circle',
				symbolSize: 6,
				showAllSymbol: true,
				color: '#F90',
				itemStyle: {
					normal: {
						lineStyle: {
							width: 1.7
						}
					}
				},
				data: plan_data2
			}
		]
	};
	businessProgress.setOption(businessProgress_option);
}

//环境监控
function loadKyjydData() {
	var plan_data1 = [];
	var data = [];
	var plan_xAxis = [];

	for (var i = 0; i < dlData.length; i++) {
		var item = dlData[i];
		plan_xAxis.push(item.date);
		data.push(item.kyjyd);
	}

	quality = echarts.init(document.getElementById('quality'));
	quality_option = {
		xAxis: {
			data: plan_xAxis,
			axisLabel: {
				textStyle: {
					color: '#B7E1FF',
					fontSize: 15
				}
			},
			axisLine: {
				lineStyle: {
					color: '#09F'
				}
			},
			axisTick: {
				lineStyle: {
					color: '#09F'
				}
			}
		},
		yAxis: {
			min: 900000,
			inverse: false,
			splitArea: { show: false },
			axisLine: { show: false },
			axisTick: { show: false },
			axisLabel: {
				textStyle: {
					color: '#B7E1FF',
					fontSize: 15,
					fontFamily: 'Arial',
				}
			},
			splitLine: {
				lineStyle: {
					color: '#09F'
				}
			}
		},
		tooltip: {
			trigger: 'axis',
			textStyle: {
				color: '#FFF',
				fontSize: 20
			}
		},
		grid: {
			left: 100
		},
		legend: {
			show: false,
			top: 'bottom',
			textStyle: {
				color: '#F00',
				fontSize: 24
			},
			data: ['电量(千瓦时)']
		},
		series: [
			{
				name: '电量',
				type: 'line',
				smooth: true,
				symbol: 'circle',
				symbolSize: 6,
				showAllSymbol: true,
				color: '#F90',
				itemStyle: {
					normal: {
						lineStyle: {
							width: 1.7
						}
					}
				},
				data: data
			}
		]
	};
	quality.setOption(quality_option);
}
//生成订单号
function getOrderNumber(n) {
	var no = "000000" + n.toString();
	return no.substring(no.length - 6);
}

// 报警与广播
function loadBjygb(){
	var data = ["广播：告别PC增长依赖，联想集团Q1迎来第二增长曲线","111111111111111","333333333"]
	var ul = document.querySelector(".marquee_list")
	var index = 0
	for(var str of data){
		var li = document.createElement("li")
		li.innerText = str
		ul.appendChild(li)
	}
	simpleScroll({
	      wrapNode: '#J-scroll',
	      line: 1,
	      duration: 1500,
	      speed: 500
	    });

}
// 公告循环滚动
	var simpleScroll = function (opts) {
	  var opts = $.extend({
	    wrapNode: '',   //父节点
	    listNode: 'ul', //列表节点
	    itemNode: 'li', //单个节点
	    speed: 500,     //滚动速度
	    duration: 2000, //间隔时间(毫秒)
	    line: 1         //滚动行数
	  }, opts);
	
	  var $list = $(opts.wrapNode).find(opts.listNode),
	    lineH = $list.find(opts.itemNode + ":first").height(),
	    upHeight = 0 - opts.line * lineH;
	
	  var scrollUp = function () {
	    $list.animate({
	      marginTop: upHeight
	    }, opts.speed, function () {
	      for (var i = 0; i < opts.line; i++) {
	        $list.find(opts.itemNode + ":first").appendTo($list);
	      }
	      $list.css({ marginTop: 0 });
	    });
	  }
	  window.setInterval(scrollUp, opts.duration);
	}


//前面补0
function insertZero(n) {
	var no = "000000" + n.toString();
	return no.substring(no.length - 2);
}

//打开模态窗口
function openDialog(DlgName) {
	$('#' + DlgName).dialog('open');
}